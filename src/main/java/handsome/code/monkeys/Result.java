package handsome.code.monkeys;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import handsome.code.monkeys.data.Event;
import handsome.code.monkeys.data.Room;
import lombok.extern.jbosslog.JBossLog;

@JBossLog
public class Result {
	private int capacity = 0;

	private long duration = 0;

	private int E = 0;

	private long endTime = 0;

	private final List<Event> events = new ArrayList<>();

	private int line = 0;

	private final String outputDir = System.getProperty("user.dir") + "/../src/main/resources/output/";

	private final Map<Room, List<Event>> roomEvents = new HashMap<>();

	private final List<Room> rooms = new ArrayList<>();

	private double score = 0;

	private final String scoreDir = System.getProperty("user.dir") + "/../src/main/resources/score/";

	private long statTime = Long.MAX_VALUE;

	public Result(Path file) {
		log.info("=== " + file + " ===");
		try (Stream<String> input = Files.lines(file);
				Stream<String> output = Files.lines(Paths.get(outputDir + file.getFileName()))) {
			/* INPUT */
			input.forEach(string -> {
				String[] strings = string.split(" ");
				if (line == 0) {
					E = Integer.valueOf(strings[0]);
				} else if (line > 0 && line < E + 1) {
					endTime = Math.max(endTime, Long.valueOf(strings[2]));
					events.add(new Event(strings[0], Long.valueOf(strings[1]), Long.valueOf(strings[2]),
							Integer.valueOf(strings[3])));
					statTime = Math.min(statTime, Long.valueOf(strings[1]));
				} else {
					capacity = Math.max(capacity, Integer.valueOf(strings[1]));
					rooms.add(new Room(strings[0], Integer.valueOf(strings[1])));
				}
				line++;
			});
			List<Event> currentEvents = new ArrayList<>();
			output.forEach(string -> {
				List<Event> eventz = new ArrayList<>();
				String[] strings = string.split(":");
				Room room = rooms.stream().filter(r -> r.name.compareTo(strings[0]) == 0).findFirst().get();
				Event previousEvent = null;
				if (strings.length > 1) {
					for (String name : strings[1].split(" ")) {
						Event currentEvent = events.stream().filter(e -> e.name.compareTo(name) == 0).findFirst().get();
						if (currentEvents.contains(currentEvent) || room.capacity < currentEvent.member
								|| previousEvent != null && previousEvent.endTime > currentEvent.statTime) {
							try {
								Files.write(Paths.get("score_" + file.getFileName().toString()), "INVALID".getBytes());
								System.out.print("INVALID\n");
								System.exit(0);
							} catch (IOException ioe) {
								ioe.printStackTrace();
							}
						}
						currentEvents.add(currentEvent);
						eventz.add(currentEvent);
						previousEvent = currentEvent;
					}
				}
				roomEvents.put(room, eventz);
			});
			/* INPUT */

			/* CODE */
			roomEvents.keySet().forEach(room -> {
				duration = 0;
				roomEvents.get(room).forEach(event -> {
					duration += event.endTime - event.statTime;
					if (room.capacity.intValue() > 0) {
						score += (double) event.member.intValue() / room.capacity.intValue()
								* (event.endTime - event.statTime);
					}
				});
				score -= (double) room.capacity.intValue() / capacity * (endTime - statTime - duration);
			});
			/* CODE */

			/* OUTPUT */
			Files.write(Paths.get(scoreDir + file.getFileName()), String.valueOf(score).getBytes());
			log.info(String.valueOf(Math.round(score)) + "\n");
			/* OUTPUT */
		} catch (IOException e) {
			log.error(e);
		}
	}
}
